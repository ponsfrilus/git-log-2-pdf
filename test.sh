#!/bin/sh

[ -d test ] && rm -rf test
[ -d tmp ] || mkdir tmp

mkdir test
cd test
git init .

echo "one" > 01.txt
git add 01.txt
git commit -m "Add 01"

echo "two" > 02.txt
echo "three" >> 01.txt
git add 0[12].txt
git commit -m "Add 02.txt and append three to 01"

echo "four" >> 02.txt
git add 02.txt
git commit -m "Append four to 02"

sleep 1s
git checkout -b feat1
echo "five" > 03.txt
echo "six"  > 04.txt
echo "seven" >> 02.txt
git add 0[234].txt
git commit -m "Add 03, 04 and modify 02"

echo "eight" >> 01.txt
git add 01.txt
git commit -m "Append eight to 01"

echo "eight" >> 01.txt
git add 01.txt
git commit -m "Append eight again to 01"


sleep 2s
git checkout -b feat2
echo "5" >> 03.txt
echo "6" >> 04.txt
echo "7" >> 02.txt
git add 0[234].txt
git commit -m "Modify 02, 03 and 04"

git checkout feat1
git merge feat2 --no-ff -m "Merging feat2"

git checkout master
echo "nine" > 05.txt
git add 05.txt
git commit -m "Add 05"

echo "ten" >> 05.txt
git add 05.txt
git commit -m "Append ten to 05"

git merge --no-ff -m "Simple merging of feat1" feat1

sleep 2s
echo "eleven" >> 05.txt
git add 05.txt
cat << EOF | git commit -F -
Append eleven to 05

this is a body commit message
with multiple lines
EOF


git log --branches --graph --oneline --parents

ruby ../git2pdf.rb -g -F > ../tmp/aaa.tex
cd ../tmp
pdflatex aaa.tex && pdflatex aaa.tex && open aaa.pdf 

cd ../test
git lg
xdg-open ../tmp/aaa.pdf
