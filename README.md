# git-log-2-pdf

Project to create a tool that can generate a report/work diary based on `git log`.


# Keywords
gitlog2pdf, git2pdf, git2report, git2workdiary, etc...


# General idea
One wants to be able to give his manager a report of his day's activities. For
that, he just runs a cmd in his git repository to create a report of the project
 that can contain:
  - Project description
  - General statistics
  - Detailed commit messages of the day or week
  - A colorfull commit graph
  - Older commit messages


# Commands
This § aim to design how to use the tool, from a CLI user POV:

| :Command: | :Description: |
| --------- | ------------- |
| `gl2p`    | Generate a report with default option |
| `gl2p --nocover` | Generate a report without the cover page |
| `gl2p --stats` | Generate a report with the repository statistics |
| `gl2p -o report.pdf` | Generate a report with specifying the ouptut filename |
| `gl2p --since 2019-01-01` | Generate a report since 1st january |
| `gl2p --from 2019-01-01 --until 2019-01-31` | Generate a report from 1st January to 31st January |


# Resources

A very good start is https://git.wiki.kernel.org/index.php/InterfacesFrontendsAndTools

Also, the command `git instaweb --httpd webrick` can gives some idea.

## Similar projects
* https://sourceforge.net/projects/git-reports/ / https://github.com/edgardleal/git-reports / http://kevinsawicki.github.io/git-reports/egit-1.3.html
* https://coderwall.com/p/bcthew/create-your-work-report-using-git-log
* https://github.com/vanhtuan0409/git-report
* https://github.com/bill-auger/git-branch-status
* https://github.com/T3kstiil3/github_report_dashboard
* https://github.com/masukomi/git-status-report
* https://github.com/lukecampbell/github-reporting
* https://github.com/knugie/git_report
* https://github.com/novoda/github-reports
*
*


## Search
* https://stackoverflow.com/questions/6489088/generate-a-pdf-logbook-from-git-commits
    * http://www.importsoul.net/python/pygitbook/
    * https://github.com/Hugoagogo/pyGitBook
* https://stackoverflow.com/questions/1057564/pretty-git-branch-graphs
* https://github.com/Hightor/gitlog
    * http://mirrors.ibiblio.org/CTAN/macros/latex/contrib/gitlog/gitlog.pdf
* https://coderwall.com/p/bcthew/create-your-work-report-using-git-log
* http://marklodato.github.io/visual-git-guide/index-en.html?no-svg

## Graphs
* https://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git
* http://bit-booster.com/best.html
* http://gitgraphjs.com/
* https://github.com/esc/git-big-picture

## Icons
* https://icons8.com/icons/set/git-branch

## Terminal to image
* https://unix.stackexchange.com/a/41211/169984 `convert label:"$(git log)" result.png`

## Statistics
* `git shortlog -s -n`
* https://stackoverflow.com/questions/1828874/generating-statistics-from-git-repository
* http://gitstats.sourceforge.net/
* https://jgehring.github.io/pepper/gallery/

## LaTex
* https://tex.stackexchange.com/questions/125244/how-can-i-produce-the-history-graph-of-a-git-repository-in-latex
* gitinfo2: http://www.pirbot.com/mirrors/ctan/macros/latex/contrib/gitinfo2/gitinfo2.pdf
* gitlog: http://www.pirbot.com/mirrors/ctan/macros/latex/contrib/gitlog/gitlog.pdf
* https://mirror.hmc.edu/ctan/macros/latex/contrib/gitlog/gitlog.pdf
* tabular format in latex git log: https://github.com/ypid/typesetting/tree/master/scripts/latex-git-log
* Tikz to PNG: https://tex.stackexchange.com/questions/13349/tikz-to-non-pdf
* Shell script in LaTeX: https://stackoverflow.com/questions/3252957/how-to-execute-shell-script-from-latex
